# Im Projekt bestehendes Code Design Ruleset


## Notation

### Variable 

Variablen werden in `snake_case` definiert:
```java
var  temp_variable_that_represents_this = SomeThing
```

Dabei sollte beachtet werden, dass die Bennenung die Information der Variable beschreiben soll.

```java
var cycle_counter = 0 // OR
var cycle = 0 // If there is only one usecase available

// Examples
var entry_id = "234ieoA4"
var entry_name = "Foo"

var server_address = "127.0.0.1"
var temp_sum = a + b
```

#### Design Convention

Variablen die als Funktionsparameter definiert werden.
Werden als "temporär"(als funktionsparameter mit `t_`) markiert.

```js
function foo(t_foo_name, t_foo_id){
    var foo_name = t_foo_name;
    return foo(foo_name)
}
```

Variablen die Member einer Klasse sind werden mit `m_` "Member" markiert.

```js
class Foo{
    var m_foo_name = "foooooooo"
    var m_foo_id = 0
}
```




### Functions/Methods

Methoden werden in `camelCase` definiert.

```js
function foooFooFoooo(){
    doOnFoo()
}

function doOnFoo(){

}
```

Die Bennenung der Funktion sollte beschreiben was dort passiert oder Anlass auf den Rückgabewert geben.

```js
function main(){
    if (isFoo()){// Checking by boolean isSomething()?
        doFoo(getFoo())// doSomething() on getSomething()

        setFoo("foooo")// setSomething()
    }
}

function isFoo(){
    return true
}

function doFoo(t_foo){
    print(t_foo)
}

function getFoo(){
    return "FOOOOOOOOOOOOOOOOOO"
}

function setFoo(t_foo){
    foo = t_foo
}

// Examples
function receiveData(){}
function sendData(){}
function isError(t_msg){}
function isBool(t_var){}
function getSum(t_a, t_b){}
```

